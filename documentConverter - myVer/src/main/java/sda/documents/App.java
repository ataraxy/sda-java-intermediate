package sda.documents;

import sda.documents.conventers.DocumentConverter;
import sda.documents.conventers.IDocumentConverter;
import sda.documents.exceptions.FileReaderException;
import sda.documents.exceptions.FileWriterException;

public class App {
    public static void main(String[] args) throws FileWriterException {

        //wszystkie readery musza implementowac IFileReader
        //dopisac implementacje fabryki filereadyerow zeby wyciagala rozszerzenie z sciezki plikow
        //dopisac implementacje filereadera

        String inputFilePath =
                "D:\\IdeaProjects\\sda\\zajecia\\michalbasinski\\java-intermediate\\" +
                        "documentConverter - myVer\\src\\main\\resources\\input.csv";
        String outputFilePath =
                "D:\\IdeaProjects\\sda\\zajecia\\michalbasinski\\java-intermediate\\" +
                        "documentConverter - myVer\\src\\main\\resources\\converted\\output.json";

        IDocumentConverter documentConverter = new DocumentConverter();
        System.out.println(" ");
        try {
            documentConverter.convert(inputFilePath, outputFilePath);
        } catch (FileReaderException e) {
            e.printStackTrace();
        }

    }
}
