package sda.documents.writers;

import sda.documents.writers.CSVFileWriter;
import sda.documents.writers.IFileWriter;

public class FileWriterFactory {
    public IFileWriter produce(String filePath) {
        IFileWriter result = null;
        if (filePath.endsWith(".csv")) {
            result = new CSVFileWriter();
        }

        if (filePath.endsWith(".json")) {
            result = new JSONFileWriter();
        }
        return result;
    }

}
