package sda.documents.writers;

import sda.documents.exceptions.FileWriterException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CSVFileWriter implements IFileWriter {

    @Override
    public void write(String filePath, List<Map<String, String>> data) throws FileWriterException {
        try {
            FileWriter fileWriter = new FileWriter(filePath);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            List<String> rows = prepareRows(data);

            for (String row : rows) {
                bufferedWriter.write(row + '\n');
            }

            bufferedWriter.close();
            fileWriter.close();     // dodaje sie na serwerach, zeby polaczenie do plikow nie bylo utrzymywane

        } catch (IOException e) {
            throw new FileWriterException(e.getMessage(), e);
        }

    }

    private List<String> prepareRows(List<Map<String, String>> data) {
        List<String> result = new ArrayList<>();
        //TODO: 1.utworzyć listę zawierającą wiersze nowego pliku CSV
        //TODO: 1.wybrac dowolny element z listy "data"

        List<String> headers = new ArrayList<>();
        for (String key : data.get(0).keySet()) {
            headers.add(key);
        }
        String headerRow = "";
        for (String header : headers) {
            headerRow = headerRow + header + ";";
        }
        result.add(headerRow);

        //przeiterowanie sie po liscie "data"
        for (Map<String, String> rowData : data) {

            String row = "";
            for (String header : headers) {
                row = row.concat(rowData.get(header) + ";");
                //TODO: sklejenie wartości wiersza + dodanie go do listy result
            }
            result.add(row);
        }

        //TODO: 2.utworzyc z tego lite naglowkow przy uzyciu metody .keySet(); - mozna uzyc do tego dowolny element z listy
        //TODO: bo wszystkie mapy na liscie maja takie same klucze
        //TODO: 3. dla kazdej mapy pobrać w pętli wartości używając kluczy i scalić je w jeden string
        return result;
    }

}