package sda.ex;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class App
{
    public static void main( String[] args ) throws IOException {
        BufferedReader reader
                = new BufferedReader(new InputStreamReader(System.in));

        IMachine vendingMachine = new VendingMachine();

        //==========WYPISANIE LISTY PRODUKTÓW I KODÓW==============
        Map<String, Dispenser> choicesMap =
                vendingMachine.getContainer().getStock();

        for (String key : choicesMap.keySet()) {
            Product product = choicesMap.get(key).getProduct();
            System.out.println(key + " "+product.getName()+" "+formatNumber(product.getPrice()));
        }
        //==========================================================


        System.out.println("Wybierz produkt:");
        String choice = reader.readLine();
        vendingMachine.readChoice(choice);
        int productPrice = vendingMachine.getPaymentAmount();
        System.out.println("Kwota do zapłaty " + formatNumber(productPrice) + " zł.");

        System.out.println("Zapłać za towar.");
        boolean paid = false;
        while (paid == false) {
            String coin = reader.readLine();
            int amount = Integer.parseInt(coin);
            vendingMachine.insertCoin(getCoin(coin));
            paid = vendingMachine.isEnough();
        }
        System.out.println("Wydano produkt");

        vendingMachine.prepareRest();

    }

    private static Coins getCoin(String coin) {
        return Coins.TWENTY_GR;
    }

    private static float formatNumber(int number) {
        return (float) number/100;
    }
}
