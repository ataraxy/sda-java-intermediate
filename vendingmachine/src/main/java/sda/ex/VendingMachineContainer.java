package sda.ex;

import java.util.LinkedHashMap;
import java.util.Map;

public class VendingMachineContainer implements IContainer {

    Map<String, Dispenser> stock;

    public VendingMachineContainer() {
        this.stock = createStock();
    }

    private Map<String, Dispenser> createStock() {
        Map<String, Dispenser> stockMap = new LinkedHashMap<>();

        Product product1 =
                new Product("Coca cola",199,ProductType.DRINK);
        stockMap.put("4F", new Dispenser(product1, 10));

        Product product2 =
                new Product("Coca cola light",199,ProductType.DRINK);
        stockMap.put("22", new Dispenser(product2, 10));

        Product product3 =
                new Product("Snickers",250,ProductType.SWEET);
        stockMap.put("5A", new Dispenser(product3, 10));

        return stockMap;
    }

    @Override
    public int getPriceOfProduct(String id) {
        Dispenser dispenser = stock.get(id);
        Product product = dispenser.getProduct();
        int price = product.getPrice();
        return price;
        //return stock.get(id).getProduct().getPrice();
    }

    @Override
    public Map<String, Dispenser> getStock() {
        return stock;
    }
}
