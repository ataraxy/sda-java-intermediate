package sda.cards;

import sda.cards.interfaces.ICardValidator;
import sda.cards.interfaces.IIssuerDetector;
import sda.cards.interfaces.ILuhnValidator;

public class SDACardValidator implements ICardValidator {
    public ValidationResult validateCardNo(String cardNo) {
        ValidationResult result = new ValidationResult();

        IIssuerDetector detector = new IssuerDetectorImpl();
        String issuer = detector.detectIssuer(cardNo);

        ILuhnValidator validator = new LuhnValidatorImpl();
        boolean isLuhnPassed = validator.isCorrect(cardNo);

        result.setIssuer(issuer);
        result.setLuhnPassed(isLuhnPassed);
        return result;
    }
}

//
// java App 1111111 C:/baza.txt