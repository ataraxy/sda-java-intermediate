package sda.cards;

import sda.cards.interfaces.ICardValidator;

public class App
{
    public static void main( String[] args ) {
        ICardValidator validator = new SDACardValidator();
        ValidationResult result = validator.validateCardNo("18606");
        System.out.println("ISSUER: " + result.getIssuer());
        System.out.println("LUHN PASSED:" + result.isLuhnPassed());
    }
}
