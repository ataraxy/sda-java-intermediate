package sda.cards;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Zadaniem klasy jest otwarcie pliku, przetworzenie go linia po linii oraz stworzenie listy zawierającej reguły typu IssuerRule
 */

public class IssuerRuleBuilder {
    public List<IssuerRule> prepareRules(){
        String fileWithRules = "D:\\IdeaProjects\\validator\\src\\main\\resources\\validate rules.txt";

        FileReader reader = new FileReader();

        List<String> lines = reader.readLinesFromTheFile(fileWithRules);

        List<IssuerRule> result = new ArrayList<>();

        for (String line : lines){
            String[] tokens = line.split(";");
            IssuerRule rule = new IssuerRule(tokens[0], Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]));
            result.add(rule);
        }

        return result;
    }
}
