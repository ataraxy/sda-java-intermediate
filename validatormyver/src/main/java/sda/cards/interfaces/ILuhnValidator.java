package sda.cards.interfaces;

public interface ILuhnValidator {
    boolean isCorrect(String cardNo);
}
