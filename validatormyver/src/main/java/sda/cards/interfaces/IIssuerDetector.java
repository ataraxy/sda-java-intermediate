package sda.cards.interfaces;

public interface IIssuerDetector {
    String detectIssuer(String cardNo);
}
