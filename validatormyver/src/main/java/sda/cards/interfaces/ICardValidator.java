package sda.cards.interfaces;

import sda.cards.ValidationResult;

public interface ICardValidator {
    ValidationResult validateCardNo(String cardNo);
}
