package sda.cards;

import sda.cards.interfaces.IIssuerDetector;

import java.util.List;

/**
 * Klasa ma za zadanie wywołać IssuerRuleBuildera, zbudować listę reguł i przyrównać regułę do numeru karty.
 * Jeśli reguła zostanie dopasowana do numeru karty, metoda detect issuer ma zazadanie zwrócić nazwę issuera
 */
public class IssuerDetectorImpl implements IIssuerDetector {
    @Override
    public String detectIssuer(String cardNo) {
        IssuerRuleBuilder ruleBuilder = new IssuerRuleBuilder();
        List<IssuerRule> rules = ruleBuilder.prepareRules();

        String result = "unknown";

        for(IssuerRule rule : rules) {
            //TODO: sprawdzić czy prefix i długość się zgadzają - jeśli tak to zwracamy name
            if(cardNo.startsWith(String.valueOf(rule.getPrefix())) && cardNo.length() == rule.getLength()) {
                result = rule.getName();
            }
        }

        return result;
    }
}
