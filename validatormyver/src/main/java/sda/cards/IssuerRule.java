package sda.cards;

public class IssuerRule {
    String name;
    int prefix;
    int length;

    public IssuerRule(String name, int prefix, int length) {
        this.name = name;
        this.prefix = prefix;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrefix() {
        return prefix;
    }

    public void setPrefix(int prefix) {
        this.prefix = prefix;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
