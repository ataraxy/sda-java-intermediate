package sda.cards;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileReader {
    public static List<String> readLinesFromTheFile(String path){
        List<String> result = null;
        try {
            result = Files.readAllLines(Paths.get(path));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }
}
