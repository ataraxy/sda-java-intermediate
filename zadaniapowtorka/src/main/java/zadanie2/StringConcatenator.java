package zadanie2;

public interface StringConcatenator {
    String concatenate(String first, String second);
}
