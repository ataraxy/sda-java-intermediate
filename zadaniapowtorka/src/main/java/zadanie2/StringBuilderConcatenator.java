package zadanie2;

public class StringBuilderConcatenator implements StringConcatenator {


    @Override
    public String concatenate(String first, String second) {

        StringBuilder sb = new StringBuilder();

        sb.append(first);
        sb.append(second);
        System.out.println(sb);

        return sb.toString();
    }
}
