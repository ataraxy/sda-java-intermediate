package sda.design.patterns.ex2;

public class DogToCatAdapter extends Cat{
    private Dog dog;

    public DogToCatAdapter(Dog dog){
        this.dog = dog;
    }

    @Override
    public String meow() {
        return dog.woof();
    }
}
