package sda.pres.card.factory;

import sda.pres.card.issuers.IRuleBuilder;
import sda.pres.card.issuers.IssuerRule;
import sda.pres.card.issuers.impl.IssuerRuleBuilder;
import sda.pres.card.issuers.impl.IssuerRuleFromFileBuilder;

import java.util.ArrayList;
import java.util.List;

public class IssuerRuleListFactory {

    public List<IssuerRule> create(String path){
        IRuleBuilder ruleBuilder = null;
        if (path != null && !path.isEmpty()) {
            ruleBuilder = new IssuerRuleFromFileBuilder(path);
        } else {
            ruleBuilder = new IssuerRuleBuilder();
        }
        return ruleBuilder.buildRules();
    }
}
